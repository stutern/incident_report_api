# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/incident_categories', type: :request do
  let!(:incident_categories) { create_list(:incident_category, 10) }
  let(:valid_attributes) { { name: incident_categories.first.name } }
  let(:incident_category_id) { incident_categories.first.id }
  let(:invalid_attributes) { { name: Faker::Number.digit } }
  let(:valid_headers) { { headers: { 'ACCEPT' => 'application/json' } } }

  describe 'GET /index' do
    before { get api_v1_incident_categories_url, as: :json }
    it 'renders a successful response' do
      expect(response).to be_successful
      expect(JSON.parse(response.body)).to_not be_empty
    end

    it 'returns resource categories' do
      expect(JSON.parse(response.body).size).to eq(10)
    end
  end

  describe 'GET /show' do
    before { get api_v1_incident_categories_url(valid_attributes), as: :json }
    it 'renders a successful response' do
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    let(:valid_attribute) { { incident_category: valid_attributes } }
    let(:invalid_attribute) { { incident_category: invalid_attributes } }

    context 'with valid parameters' do
      before { post api_v1_incident_categories_url, params: valid_attribute, as: :json }
      it 'creates a new incident category' do
        expect(response['name']).to eq(valid_attribute['name'])
      end

      it 'renders a JSON response with the new incident category' do
        expect(response).to have_http_status(:created)
        expect(response).to have_http_status(201)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      before { post api_v1_incident_categories_url, params: invalid_attribute, as: :json }
      it 'does not create a new incident category' do
        expect { response }.to change(IncidentCategory, :count).by(0)
      end

      it 'renders a JSON response with errors for the new incident_category' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/only letters are allowed/)
      end
    end
  end

  describe 'PATCH /update' do
    let(:valid_attribute) { { name: 'Sars' } }
    let(:invalid_attribute) { { incident_category: invalid_attributes } }

    context 'with valid parameters' do
      before { put "/api/v1/incident_categories/#{incident_category_id}", params: valid_attribute, as: :json }

      it 'updates the incident category name' do
        expect(response['name']).to eq(valid_attribute['name'])
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'renders a JSON response with the incident category' do
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to include('application/json')
      end
    end

    context 'with invalid paramters' do
      before { put "/api/v1/incident_categories/#{incident_category_id}", params: invalid_attribute, as: :json }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/only letters are allowed/)
      end

      it 'renders a JSON response with errors for the incident category' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to include('application/json')
      end
    end
  end

  describe 'DELETE /destroy' do
    before { delete "/api/v1/incident_categories/#{incident_category_id}", as: :json }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
