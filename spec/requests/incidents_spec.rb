# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/incidents', type: :request do
  let!(:users) { create_list(:user, 10) }
  let!(:incidents) { create_list(:incident, 10, user_id: users.first.id) }
  let(:valid_attributes) do
    {
      incident_category_id: incidents.first.incident_category_id,
      user_id: incidents.first.user_id,
      description: incidents.first.description
    }
  end

  let(:invalid_attributes) do
    { 
      description: Faker::Lorem.sentences, incident_category_id: nil
    }
  end

  let(:incident_id) { incidents.first.id }
  let(:user_id) { incidents.first.user_id }
  let(:user_email) { incidents.first.user.email }
  let(:user_token) { incidents.first.user.authentication_token }

  describe 'GET /index' do
    before do
      get "/api/v1/users/#{user_id}/incidents?user_email=#{user_email}
          &user_token=#{user_token}",
      as: :json
    end

    it 'returns a successful response' do
      expect(response).to be_successful
      expect(response).to have_http_status(:success)
    end
    it 'renders all incident reports' do
      expect(JSON.parse(response.body).size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /show' do
    before do 
      get "/api/v1/users/#{user_id}/incidents/#{incident_id}?user_email=#{user_email}
          &user_token=#{user_token}", as: :json
    end

    it 'renders a successful response' do
      expect(response).to be_successful
    end

    it 'renders an incident report user' do
      expect(JSON.parse(response.body)['user_id']).to eq(valid_attributes[:user_id])
    end
  end

  describe 'POST /create' do
    let(:valid_attribute) { { incident: valid_attributes } }
    let(:invalid_attribute) { { incident: invalid_attributes } }

    context 'with valid parameters' do
      before do 
        post "/api/v1/users/#{user_id}/incidents?user_email=#{user_email}
        &user_token=#{user_token}", params: valid_attribute, as: :json
      end

      it 'creates a new incident report' do
        expect(response['description']).to eq(valid_attribute['name'])
      end

      it 'renders a JSON response with the new incident report' do
        expect(response).to have_http_status(:created)
        expect(response).to have_http_status(201)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      before do 
        post "/api/v1/users/#{user_id}/incidents?user_email=#{user_email}
      &user_token=#{user_token}", params: invalid_attribute, as: :json
      end

      it 'does not create a new incident report' do
        expect { response }.to change(Incident, :count).by(0)
      end

      it 'renders a JSON response with errors for the new incident' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including('application/json'))
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/can't be blank/)
      end
    end
  end

  describe 'PATCH /update' do
    let(:valid_attribute) do
      {
        incident_category_id: incidents.last.incident_category_id
      }
    end
    let(:invalid_attribute) do
      {
        incident: invalid_attributes
      }
    end

    context 'with valid parameters' do

      before do 
        put "/api/v1/users/#{user_id}/incidents/#{incident_id}?user_email=#{user_email}
        &user_token=#{user_token}", params: valid_attribute, as: :json
      end

      it 'updates the incident report category' do
        expect(response['incident_category_id']).to eq(valid_attribute['incident_category_id'])
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'renders a JSON response with the incident category' do
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to include('application/json')
      end
    end

    context 'with invalid parameters' do
      
      before do 
        put "/api/v1/users/#{user_id}/incidents/#{incident_id}?user_email=#{user_email}
        &user_token=#{user_token}", params: invalid_attribute, as: :json
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body).to match(/can't be blank/)
      end

      it 'renders a JSON response with errors for the incident report' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to include('application/json')
      end
    end
  end

  describe 'DELETE /destroy' do
    before do
      delete "/api/v1/users/#{user_id}/incidents/#{incident_id}?user_email=#{user_email}
      &user_token=#{user_token}", as: :json
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
