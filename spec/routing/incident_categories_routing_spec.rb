# frozen_string_literal: true
# require "rails_helper"

# RSpec.describe IncidentCategoriesController, type: :routing do
#   describe "routing" do
#     it "routes to #index" do
#       expect(get: "/incident_categories").to route_to("incident_categories#index")
#     end

#     it "routes to #show" do
#       expect(get: "/incident_categories/1").to route_to("incident_categories#show", id: "1")
#     end

#     it "routes to #create" do
#       expect(post: "/incident_categories").to route_to("incident_categories#create")
#     end

#     it "routes to #update via PUT" do
#       expect(put: "/incident_categories/1").to route_to("incident_categories#update", id: "1")
#     end

#     it "routes to #update via PATCH" do
#       expect(patch: "/incident_categories/1").to route_to("incident_categories#update", id: "1")
#     end

#     it "routes to #destroy" do
#       expect(delete: "/incident_categories/1").to route_to("incident_categories#destroy", id: "1")
#     end
#   end
# end
