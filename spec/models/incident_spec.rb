# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Incident, type: :model do
  before(:all) do
    @incident = create(:incident)
  end

  it 'should be valid with valid attributes' do
    expect(@incident).to be_valid
  end

  it 'should not be valid without a description' do
    @incident.description = nil
    expect(@incident).to_not be_valid
  end

  it 'should not be valid without a category' do
    @incident.incident_category_id = nil
    expect(@incident).to_not be_valid
  end

  it 'should not be valid without a user' do
    @incident.user = nil
    expect(@incident).to_not be_valid
  end

  it 'should be valid with an image' do
    @incident = create(:incident)
    @incident.image = 'https://img.com'
    expect(@incident).to be_valid
  end

  it 'should should be valid with the valid user' do
    @incidents = create_list(:incident, 2)
    @incidents.first.user_id = User.find(@incidents.first.user_id).id
    expect(@incidents.first).to be_valid
  end
end
