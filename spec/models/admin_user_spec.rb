# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdminUser, type: :model do
  before(:all) do
    @admin_user = create(:admin_user)
  end

  it 'should have valid attributes' do
    expect(@admin_user).to be_valid
  end

  it 'should not be valid without email' do
    @admin_user.email = nil
    expect(@admin_user).to_not be_valid
  end

  it 'should not be valid without password' do
    @admin_user.password = nil
    expect(@admin_user).to_not be_valid
  end
end
