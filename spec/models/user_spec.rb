# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  before(:all) do
    @user = create(:user)
  end

  it 'should have valid attributes' do
    expect(@user).to be_valid
  end

  it 'should not be valid without email' do
    @user.email = nil
    expect(@user).to_not be_valid
  end

  it 'should not be valid without password' do
    @user.password = nil
    expect(@user).to_not be_valid
  end
end
