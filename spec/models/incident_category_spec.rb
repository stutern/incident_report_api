# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IncidentCategory, type: :model do
  before(:all) do
    @incident_category = create(:incident_category)
  end

  it 'should have valid attributes' do
    expect(@incident_category).to be_valid
  end

  it 'should not be valid without name' do
    @incident_category.name = nil
    expect(@incident_category).to_not be_valid
  end

  it 'should have a unique name' do
    @incident_category.name = 'Fire outbreak'
    @incident_category_2 = create(:incident_category, name: @incident_category.name)
    expect(@incident_category).to_not be_valid
  end

  it 'should have a unique case insensitive name' do
    @incident_category.name = 'Fire outbreak'
    @incident_category_2 = create(:incident_category, name: @incident_category.name.downcase)
    expect(@incident_category).to_not be_valid
  end
end
