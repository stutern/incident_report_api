# frozen_string_literal: true

FactoryBot.define do
  factory :incident_category do
    name { Faker::Superhero.prefix }
  end
end
