# frozen_string_literal: true

FactoryBot.define do
  factory :incident do
    description { Faker::Lorem.sentences }
    association :incident_category
    association :user
  end
end
