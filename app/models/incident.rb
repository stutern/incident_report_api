# frozen_string_literal: true

class Incident < ApplicationRecord
  belongs_to :incident_category
  belongs_to :user

  validates_presence_of :description, :incident_category
end
