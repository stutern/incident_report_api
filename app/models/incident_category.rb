# frozen_string_literal: true

class IncidentCategory < ApplicationRecord
  has_many :incidents
  validates_presence_of :name
  validates :name, format: { with: /[a-zA-Z]/, message: 'only letters are allowed' }
  validates_uniqueness_of :name, case_sensitive: false
end
