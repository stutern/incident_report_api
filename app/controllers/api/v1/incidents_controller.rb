# frozen_string_literal: true

class Api::V1::IncidentsController < ApplicationController
  # before_action :authenticate_api_v1_admin_user!
  before_action :authenticate_api_v1_user!
  before_action :set_incident, only: %i[show update destroy]
  before_action :set_user, only: :index

  respond_to :json

  def_param_group :incident do
    param :incident, Hash, desc: 'Incident report information', required: true, action_aware: true do
      param :description, String, required: true, action_aware: true, desc: 'Report of the incident'
      param :incident_category_id, :number, required: true, desc: 'Category/type of the incident'
      param :image, String, desc: 'Image of the incident'
    end
  end

  # GET /api/v1/users/:user_id/incidents
  api :GET, '/users/:user_id/incidents', 'Index all incident reports of a user'
  def index
    @incidents = @user.incidents
    render json: @incidents
  end

  # GET /api/v1/users/:user_id/incidents/1
  api :GET, '/users/:user_id/incidents/:id', 'Shows a specific incident report of a user'
  def show
    render json: @incident
  end

  # POST /api/v1/users/:user_id/incidents
  api :POST, '/users/:user_id/incidents', 'Creates an incident report with specified params'
  param_group :incident
  def create
    @incident = Incident.new(incident_params)
    @incident.user_id = current_api_v1_user.id

    if @incident.save
      render json: @incident, status: :created
    else
      render json: @incident.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/users/:user_id/incidents/1
  api :PUT, '/users/:user_id/incidents/:id', 'Updates a specific incident report with specified params'
  param_group :incident
  def update
    if @incident.update(incident_params)
      render json: @incident
    else
      render json: @incident.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/users/:user_id/incidents/1
  api :DELETE, '/users/:user_id/incidents/:id', 'Destroys a specific incident report'
  def destroy
    @incident.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_incident
    @incident = User.find(params[:user_id]).incidents.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def incident_params
    params.require(:incident).permit(:incident_category_id, :description, :image)
  end

  def set_user
    @user = User.find(params[:user_id])
  end
end
