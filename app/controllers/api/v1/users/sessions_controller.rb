# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController
  before_action :sign_in_params, only: :create
  before_action :load_user, only: :create

  # def_param_group :user do
  #   param :user, Hash, desc: "User information", required: true, action_aware: true do
  #     param :email, String, desc: "Email of the user", required: true, action_aware: true
  #     param :password, String, desc: "Password of the user", required: true
  #   end
  # end

  # sign in
  api :POST, '/users/sign_in', 'Signs in a user'
  param_group :user, Api::V1::Users::RegistrationsController
  def create
    if @user.valid_password?(sign_in_params[:password])
      sign_in 'user', @user
      render json: {
        messages: 'Signed In Successfully',
        is_success: true,
        data: { user: @user }
      }, status: :ok
    else
      render json: {
        messages: 'Signed In Failed - Unauthorized',
        is_success: false,
        data: {}
      }, status: :unauthorized
    end
  end

  private

  def sign_in_params
    params.require(:user).permit(:email, :password)
  end

  def load_user
    @user = User.find_for_database_authentication(email: sign_in_params[:email])
    if @user
      @user
    else
      render json: {
        messages: 'Cannot get User',
        is_success: false,
        data: {}
      }, status: :failure
    end
  end
end
