# frozen_string_literal: true

class Api::V1::Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :ensure_params_exist, only: :create

  skip_before_action :verify_authenticity_token, only: :create, raise: false

  def_param_group :user do
    param :user, Hash, desc: 'User information', required: true, action_aware: true do
      param :email, String, desc: 'Email of the user', required: true, action_aware: true
      param :password, String, desc: 'Password of the user', required: true
    end
  end

  # POST /resource
  api :POST, '/users', 'Signs up a new user'
  param_group :user
  def create
    user = User.new configure_sign_up_params
    if user.save
      render json: {
        messages: 'Signed up successfully',
        is_success: true,
        data: { user: user }
      }, status: :ok
    else
      render json: {
        messages: 'Sign up failed',
        is_success: false,
        data: {}
      }, status: :unprocessable_entity
    end
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    params.require(:user).permit(:email, :password)
  end

  def ensure_params_exist
    return if params[:user].present?

    render json: {
      messages: 'Missing params',
      is_success: false,
      data: {}
    }, status: :bad_request
  end
end
