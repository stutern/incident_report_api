# frozen_string_literal: true

class Api::V1::IncidentCategoriesController < ApplicationController
  before_action :authenticate_api_v1_user!
  before_action :set_incident_category, only: %i[show update destroy]
  respond_to :json

  def_param_group :incident_category do
    param :incident_category, Hash, desc: 'Incident category information', required: true, action_aware: true do
      param :name, String, desc: 'Category/type of the incident, must be unique', required: true
    end
  end

  # GET /api/v1/incident_categories
  api :GET, '/incident_categories', 'Index all incident categories'
  def index
    @incident_categories = IncidentCategory.all

    render json: @incident_categories
  end

  # GET /api/v1/incident_categories/1
  api :GET, '/incident_categories/:id', 'Shows specific incident category'
  def show
    render json: @incident_category
  end

  # POST /api/v1/incident_categories
  api :POST, '/incident_categories', 'Creates an incident category with specified params'
  param_group :incident_category
  def create
    @incident_category = IncidentCategory.new(incident_category_params)

    if @incident_category.save
      render json: @incident_category, status: :created, content_type: 'application/json'
    else
      render json: @incident_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/incident_categories/1
  api :PUT, '/incident_categories/:id', 'Updates a specific incident category with specified params'
  param_group :incident_category
  def update
    if @incident_category.update(incident_category_params)
      render json: @incident_category
    else
      render json: @incident_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/incident_categories/1
  api :DELETE, '/incident_categories/:id', 'Destroys a specific incident category'
  def destroy
    @incident_category.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_incident_category
    @incident_category = IncidentCategory.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def incident_category_params
    params.require(:incident_category).permit(:name)
  end
end
