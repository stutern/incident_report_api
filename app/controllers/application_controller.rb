# frozen_string_literal: true

class ApplicationController < ActionController::API
  acts_as_token_authentication_handler_for User, fallback: :none
  acts_as_token_authentication_handler_for AdminUser, fallback: :none

  respond_to :json
end
