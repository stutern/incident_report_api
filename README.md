# Incident Reporter API

This API allows users report incidents (fire, outbreaks, road accidents, flood etc) that need urgent governement internvention.

## Table of contents
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Test suite](#test-suite)

## Technologies
* Ruby - version 2.7.0
* Rails - version 6.0.3.1
* Database - PostgreSQL

## Setup
* Clone the project to your machine
* Run `bundle install` commmand
* Run database `rails db:setup` command
* Seed the databse using `rails db:seed` command
* Launch the app using `rails server` command
* Open the route to `/doc` for the api documentation

## Features
* CRUD access to incdent reports
* CRUD access to incdent categories
* User authentication

To-do list: 
* Admin user authentication
* All users spec

## Status
Project is: _in progress_.

## Test suite
* You can run the test by running the `bundle exec rspec` command
