# frozen_string_literal: true

class CreateIncidents < ActiveRecord::Migration[6.0]
  def change
    create_table :incidents do |t|
      t.references :incident_category, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :description
      t.string :image

      t.timestamps
    end
  end
end
