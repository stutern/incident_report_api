# frozen_string_literal: true

class CreateIncidentCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :incident_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
