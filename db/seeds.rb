# frozen_string_literal: true

if !User.any?
  puts 'Seeding users...'
  User.create(
    [
      { email: 'user_1@example.com', password: 'password' },
      { email: 'user_2@example.com', password: 'password' },
      { email: 'user_3@example.com', password: 'password' }
    ]
  )
  puts 'Seeded user!'
end

if !IncidentCategory.any?
  puts 'Seeding incident categories...'
  IncidentCategory.create(
    [
      { name: 'Fire outbreak' },
      { name: 'Road accident' },
      { name: 'Plane crash' },
      { name: 'Flood' },
      { name: 'Robbery' }
    ]
  )
  puts 'Seeded incident categories!'
end

if !Incident.any?
  puts 'Seeding incident reports...'
  Incident.create(
    [
      { description: 'There is a pipeline explosion at Ijegun.', incident_category: IncidentCategory.first, user: User.first },
      { description: 'There is a road crash at Lekki Express Way.', incident_category: IncidentCategory.second, user: User.first },
      { description: 'There is a plane crash at Abuja Airport!', incident_category: IncidentCategory.third, user: User.first },
      { description: 'There is serious flood at Ikeja!', incident_category: IncidentCategory.fourth, user: User.first },
      { description: 'There is a mall robbery Jabi Mall right now!!!', incident_category: IncidentCategory.fifth, user: User.first }, { description: 'There is a pipeline explosion at Ijegun.', incident_category: IncidentCategory.first, user: User.second },
      { description: 'There is a road crash at Lekki Express Way.', incident_category: IncidentCategory.second, user: User.second },
      { description: 'There is a plane crash at Abuja Airport!', incident_category: IncidentCategory.third, user: User.second },
      { description: 'There is serious flood at Ikeja!', incident_category: IncidentCategory.fourth, user: User.second },
      { description: 'There is a mall robbery Jabi Mall right now!!!', incident_category: IncidentCategory.fifth, user: User.second },
      { description: 'There is a pipeline explosion at Ijegun.', incident_category: IncidentCategory.first, user: User.third },
      { description: 'There is a road crash at Lekki Express Way.', incident_category: IncidentCategory.second, user: User.third },
      { description: 'There is a plane crash at Abuja Airport!', incident_category: IncidentCategory.third, user: User.third },
      { description: 'There is serious flood at Ikeja!', incident_category: IncidentCategory.fourth, user: User.third },
      { description: 'There is a mall robbery Jabi Mall right now!!!', incident_category: IncidentCategory.fifth, user: User.third }
    ]
  )
  puts 'Seeding incident reports!'
end
