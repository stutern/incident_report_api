# frozen_string_literal: true

Apipie.configure do |config|
  config.translate               = false
  config.default_locale          = nil
  config.app_name                = 'Incident Reporter'
  config.api_base_url            = '/api/v1'
  config.doc_base_url            = '/doc'
  config.validate                = false
  config.app_info                = 'An API that allows users report incidents (fire outbreaks, road accidents, flood etc) that needs intervention.'
  config.show_all_examples       = true
  config.copyright               = "&copy; 2020 Stutern"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
