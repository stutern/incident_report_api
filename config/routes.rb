# frozen_string_literal: true

Rails.application.routes.draw do
  apipie
  namespace :api do
    namespace :v1 do
      devise_for :admin_users, controllers: {
        registrations: 'api/v1/admin_users/registrations',
        confirmations: 'api/v1/admin_users/confirmations',
        sessions: 'api/v1/admin_users/sessions'
      }
      devise_for :users, controllers: {
        registrations: 'api/v1/users/registrations',
        confirmations: 'api/v1/users/confirmations',
        sessions: 'api/v1/users/sessions'
      }
      resources :users do
        resources :incidents
      end
      resources :incident_categories
    end
  end
end
